import endpoint.Product;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static verification.ResponseVerification.statusCodeVerification;

public class ProductTest extends BaseTest {
    private static JSONObject product = new JSONObject();

    @BeforeAll
    static void init() {
        product.put("name", getRandomText());
        product.put("type", "simple");
        product.put("stock_quantity", 1);
        product.put("sku", getRandomText());
        product.put("description", getRandomText());
        product.put("short_description", getRandomText());
        product.put("regular_price", "2.00");
        product.put("status", "publish");
        product.put("featured", false);
        product.put("catalog_visibility", "visible");
    }

    @Test
    @Tag("smoke")
    @DisplayName("Create product in WooComerce")
    void createProductTest(){
        Response response = Product.sendCreateProductRequest(product);
        statusCodeVerification(201, response);
        JSONObject responseJson = new JSONObject(response.getBody().asString());
        response = Product.sendGetProductRequest(responseJson.getInt("id"));
        statusCodeVerification(200, response);
    }



}
