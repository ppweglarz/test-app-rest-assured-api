import lombok.Getter;

import java.util.UUID;

public class BaseTest {
    @Getter
    private static final String randomText = UUID.randomUUID().toString().substring(1, 15);
}
