package endpoint;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

public class Product extends WooCommerceApi {

    private static final String productUri = "/products";

    public static Response sendCreateProductRequest(JSONObject body) {
        setBasePath(productUri);
        RequestSpecification request = getRequestBase()
                .body(body.toString());
        return sendPost(request);
    }

    public static Response sendGetProductRequest(int productId) {
        setBasePath(productUri + "/" + productId);
        RequestSpecification request = getRequestBase();
        return sendGet(request);
    }

}
