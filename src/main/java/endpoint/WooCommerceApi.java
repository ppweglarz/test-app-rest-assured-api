package endpoint;

import config.Config;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public class WooCommerceApi {

    protected static Config config = new Config();
    @Setter
    private static String basePath;
    public static Map<String, String> queryParam;
    static {
        queryParam = new HashMap<>();
        queryParam.put("consumer_key", config.getApplicationUser());
        queryParam.put("consumer_secret", config.getApplicationPassword());
    }
    public static Map<String, Object> headers;
    static {
        headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
    }

    protected static RequestSpecification getRequestBase() {
        return RestAssured.given()
                .baseUri(config.getApplicationUrl())
                .basePath(basePath)
                .headers(headers)
                .queryParams(queryParam)
                .relaxedHTTPSValidation();
    }

    protected static Response sendRequest(RequestSpecification requestSpecification, Method method) {
        return requestSpecification.request(method);
    }

    protected static Response sendPost(RequestSpecification requestSpecification) {
        return sendRequest(requestSpecification, Method.POST);
    }

    protected static Response sendGet(RequestSpecification requestSpecification) {
        return sendRequest(requestSpecification, Method.GET);
    }
}
