package verification;

import io.restassured.response.Response;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResponseVerification {

    public static void statusCodeVerification(int expectedStatusCode, Response response){
        assertEquals(expectedStatusCode, response.getStatusCode());
    }
}
