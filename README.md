# WooCommerce test-app API Testing Framework

This is a demo testing framework presenting Java RestAssured possibilities. Feel free to clone or fork it.
Framework maintains some test scenarios for WooCommerce app hosted on test-app.zomago.pl

Enjoy!